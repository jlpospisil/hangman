module.exports = {
  publicPath: process.env.NODE_ENV === 'production' ? '/hangman/' : '',
  transpileDependencies: [
    'vuetify',
  ],
};
