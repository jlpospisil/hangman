const validUnits = ['px', 'rem', 'em'];

export default ({ width: defaultWidth, height: defaultHeight, color = '#000000' } = {}) => ({
  props: {
    alt: {
      type: String,
      required: false,
      default: null,
    },
    width: {
      type: [String, Number],
      required: false,
      default: null,
    },
    height: {
      type: [String, Number],
      required: false,
      default: null,
    },
    color: {
      type: String,
      required: false,
      default: null,
    },
  },
  computed: {
    svgAltText() {
      return this.alt;
    },
    svgWidth() {
      const { width, height } = this;
      const heightUnit = `${height || ''}`.replace(/^\d+/, '').toLowerCase();
      const heightValue = `${height || ''}`.replace(/\D+$/, '');
      const defaultWidthUnit = `${defaultWidth || ''}`.replace(/^\d+/, '').toLowerCase() || 'px';
      const defaultWidthValue = `${defaultWidth || ''}`.replace(/\D+$/, '');
      let widthUnit = `${width || ''}`.replace(/^\d+/, '').toLowerCase() || heightUnit;
      let widthValue = `${width || ''}`.replace(/\D+$/, '');

      if (heightValue && !widthValue) {
        widthValue = (defaultWidth / defaultHeight) * heightValue;
      }

      if (!validUnits.includes(widthUnit)) {
        widthUnit = 'px';
      }

      return widthValue ? `${widthValue}${widthUnit}` : `${defaultWidthValue}${defaultWidthUnit}`;
    },
    svgHeight() {
      const { width, height } = this;
      const widthUnit = `${width || ''}`.replace(/^\d+/, '').toLowerCase();
      const widthValue = `${width || ''}`.replace(/\D+$/, '');
      const defaultHeightUnit = `${defaultHeight || ''}`.replace(/^\d+/, '').toLowerCase() || 'px';
      const defaultHeightValue = `${defaultHeight || ''}`.replace(/\D+$/, '');
      let heightUnit = `${height || ''}`.replace(/^\d+/, '').toLowerCase() || widthUnit;
      let heightValue = `${height || ''}`.replace(/\D+$/, '');

      if (widthValue && !heightValue) {
        heightValue = (defaultHeight / defaultWidth) * widthValue;
      }

      if (!validUnits.includes(heightUnit)) {
        heightUnit = 'px';
      }

      return heightValue ? `${heightValue}${heightUnit}` : `${defaultHeightValue}${defaultHeightUnit}`;
    },
    svgColor() {
      return this.color || color;
    },
  },
});
