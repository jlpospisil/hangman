import Vue from 'vue';
import VueRouter from 'vue-router';
import HangManGame from '@/views/HangManGame.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '*',
    name: 'HangMan',
    component: HangManGame,
  },
  {
    path: '/settings',
    name: 'Settings',
    // route level code-splitting
    component: () => import(/* webpackChunkName: "settings" */ '@/views/Settings.vue'),
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
