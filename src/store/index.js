import Vue from 'vue';
import Vuex from 'vuex';
import modules from '@/store/modules';
import { persistedState } from '@/store/plugins';

Vue.use(Vuex);

export default new Vuex.Store({
  plugins: [
    persistedState,
  ],
  modules,
});
