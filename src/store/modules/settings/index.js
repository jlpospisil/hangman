import Vue from 'vue';
import * as constants from '@/store/modules/settings/constants';

export default {
  state: {
    primaryColor: null,
    words: [],
  },
  getters: {
    [constants.GET_PRIMARY_COLOR]: ({ primaryColor }) => primaryColor,
    [constants.GET_WORDS]: ({ words }) => words,
  },
  actions: {
    [constants.SET_PRIMARY_COLOR]: ({ commit }, primaryColor) => {
      commit(constants.SET_PRIMARY_COLOR, primaryColor);
    },
    [constants.SET_WORDS]: ({ commit }, words) => {
      commit(constants.SET_WORDS, words);
    },
  },
  mutations: {
    [constants.SET_PRIMARY_COLOR]: (state, primaryColor) => {
      state.primaryColor = primaryColor;
    },
    [constants.SET_WORDS]: (state, words) => {
      const wordsToSet = Array.isArray(words) ? words : [];
      Vue.set(state, 'words', wordsToSet);
    },
  },
};
