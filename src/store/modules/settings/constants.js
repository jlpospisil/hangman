export const GET_PRIMARY_COLOR = 'GET_PRIMARY_COLOR';
export const GET_WORDS = 'GET_WORDS';
export const SET_PRIMARY_COLOR = 'SET_PRIMARY_COLOR';
export const SET_WORDS = 'SET_WORDS';
